package nl.hexachess.api.service;

import lombok.RequiredArgsConstructor;
import nl.hexachess.api.model.dto.*;
import nl.hexachess.api.model.entity.Game;
import nl.hexachess.api.model.entity.Move;
import nl.hexachess.api.model.entity.User;
import nl.hexachess.api.model.request.LoginRequest;
import nl.hexachess.api.model.request.SignUpRequest;
import nl.hexachess.api.model.request.VerifyCodeRequest;
import nl.hexachess.api.model.response.VerifyCodeResponse;
import nl.hexachess.api.repository.GameRepository;
import nl.hexachess.api.repository.MoveRepository;
import nl.hexachess.api.repository.UserRepository;
import nl.hexachess.api.security.util.CookieUtil;
import nl.hexachess.api.security.util.JwtUtil;
import nl.hexachess.api.security.util.TfaUtil;
import nl.hexachess.api.util.MapperUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GameService {

    private final GameRepository gameRepository;
    private final UserRepository userRepository;
    private final MoveRepository moveRepository;
    private final NotificationService notificationService;

    public ResponseEntity<Game> getGame(Long id){
        Game game = gameRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        return ResponseEntity.ok(game);
    }

    public ResponseEntity<Game> joinGame(Long id, String username){
        Game game = gameRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        User user = userRepository.findByUsername(username).orElseThrow(IllegalArgumentException::new);
        if (game.getPlayers().size() <= 2 && game.getMode() == 1){
            game.getPlayers().add(user);
            gameRepository.save(game);
            List<Game> games = user.getGames();
            if (games == null){
                games = new ArrayList<>();
            }
            games.add(game);
            user.setGames(games);
            userRepository.save(user);
            notificationService.publish(MapperUtil.getModelMapper().map(game, GameDto.class));
            return ResponseEntity.ok(game);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(game);
    }

    public ResponseEntity<List<Game>> getGames(String username){
        User user = userRepository.findByUsername(username).orElseThrow(IllegalArgumentException::new);
        List<Game> games = user.getGames();
        return ResponseEntity.ok(games);
    }

    public ResponseEntity<?> removeGame(Long id, String username){
        User user = userRepository.findByUsername(username).orElseThrow(IllegalArgumentException::new);
        user.setGames(user.getGames().stream().filter(g -> (!g.getId().equals(id))).collect(Collectors.toList()));
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<Game> createGame(GameDto gameDto, String username){
        User user = userRepository.findByUsername(username).orElseThrow(IllegalArgumentException::new);
        Game game = Game.builder().mode(gameDto.getMode()).dateStart(new Date()).players(List.of(user)).winner(user).build();
        gameRepository.save(game);
        List<Game> games = user.getGames();
        if (games == null){
            games = new ArrayList<>();
        }
        games.add(game);
        user.setGames(games);
        userRepository.save(user);
        return ResponseEntity.ok(game);
    }

    public ResponseEntity<Game> addMove(Long id, MoveDto moveDto, String username){
        User user = userRepository.findByUsername(username).orElseThrow(IllegalArgumentException::new);
        Game game = gameRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        if (game.getPlayers().contains(user) && isPlayersTurn(user, game) || game.getPlayers().contains(user) && game.getMode() == 0){
            Move move = Move.builder().game(game).player(user).oldX(moveDto.getOldX()).oldY(moveDto.getOldY()).newX(moveDto.getNewX()).newY(moveDto.getNewY()).build();
            move = moveRepository.save(move);
            List<Move> moves = game.getMoves();
            if (moves == null){
                moves = new ArrayList<>();
            }
            moves.add(move);
            game.setMoves(moves);
            gameRepository.save(game);
            notificationService.publish(MapperUtil.getModelMapper().map(move, MoveDto.class));
            return ResponseEntity.ok(game);
        }
        return ResponseEntity.badRequest().build();
    }

    public Flux<Dto> getMoveUpdates(Long id){
        return notificationService.getSink().filter(dto -> dto instanceof MoveDto && ((MoveDto) dto).getGame().getId().equals(id));
    }

    public Flux<Dto> getJoinUpdates(Long id){
        return notificationService.getSink().filter(dto -> dto instanceof GameDto && ((GameDto) dto).getId().equals(id));
    }

    private boolean isPlayersTurn(User user, Game game){
        return !game.getMoves().isEmpty() ? game.getMoves().get(game.getMoves().size() - 1).getPlayer() != user : game.getWinner() == user;
    }
}
