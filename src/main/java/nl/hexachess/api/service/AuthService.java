package nl.hexachess.api.service;

import lombok.RequiredArgsConstructor;
import nl.hexachess.api.model.dto.TokenDto;
import nl.hexachess.api.model.entity.User;
import nl.hexachess.api.model.request.LoginRequest;
import nl.hexachess.api.model.request.SignUpRequest;
import nl.hexachess.api.model.request.VerifyCodeRequest;
import nl.hexachess.api.model.response.VerifyCodeResponse;
import nl.hexachess.api.repository.UserRepository;
import nl.hexachess.api.security.util.CookieUtil;
import nl.hexachess.api.security.util.JwtUtil;
import nl.hexachess.api.security.util.TfaUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final JwtUtil jwtUtil;
    private final CookieUtil cookieUtil;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder encoder;
    private final TfaUtil tfaUtil;

    public ResponseEntity<?> signIn(LoginRequest loginRequest, String accessToken, String refreshToken, HttpServletRequest request) {
        User user = userRepository.findByUsername(loginRequest.getUsername()).orElseThrow(IllegalArgumentException::new);
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        if (user.isMfa()){
            return ResponseEntity.status(HttpStatus.PAYMENT_REQUIRED).body(new VerifyCodeResponse(jwtUtil.generateVerificationToken(user, request), null, null));
        }

        boolean accessTokenValid = jwtUtil.validateToken(accessToken);
        boolean refreshTokenValid = jwtUtil.validateToken(refreshToken);

        HttpHeaders responseHeaders = new HttpHeaders();
        TokenDto newAccessToken;
        TokenDto newRefreshToken;

        //todo: check
        if (!accessTokenValid && !refreshTokenValid) {
            newAccessToken = jwtUtil.generateToken(user, request);
            newRefreshToken = jwtUtil.generateRefreshToken(user);
            addAccessTokenCookie(responseHeaders, newAccessToken);
            addRefreshTokenCookie(responseHeaders, newRefreshToken);
        }

        if (!accessTokenValid && refreshTokenValid) {
            newAccessToken = jwtUtil.generateToken(user, request);
            addAccessTokenCookie(responseHeaders, newAccessToken);
        }

        if (accessTokenValid && refreshTokenValid) {
            newAccessToken = jwtUtil.generateToken(user, request);
            newRefreshToken = jwtUtil.generateRefreshToken(user);
            addAccessTokenCookie(responseHeaders, newAccessToken);
            addRefreshTokenCookie(responseHeaders, newRefreshToken);
        }

        return ResponseEntity.ok().headers(responseHeaders).build();
    }

    public ResponseEntity<?> registerUser(SignUpRequest signUpRequest, HttpServletRequest request) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body("Error: Username or email address is already in use!");
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body("Error: Username or email address is already in use!");
        }
        User user = User.builder().username(signUpRequest.getUsername()).email(signUpRequest.getEmail()).password(encoder.encode(signUpRequest.getPassword())).build();
        user = userRepository.save(user);

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(signUpRequest.getUsername(), signUpRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);


        if (signUpRequest.isMfa()){
            user.setSecret(tfaUtil.generateSecret());
            user = userRepository.save(user);
            return ResponseEntity.status(HttpStatus.PAYMENT_REQUIRED).body(new VerifyCodeResponse(jwtUtil.generateVerificationToken(user, request), tfaUtil.getUriForImage(user.getSecret(), user.getUsername()), user.getSecret()));
        }

        user = userRepository.save(user);

        HttpHeaders responseHeaders = new HttpHeaders();
        TokenDto newAccessToken;
        TokenDto newRefreshToken;

        newAccessToken = jwtUtil.generateToken(user, request);
        newRefreshToken = jwtUtil.generateRefreshToken(user);
        addAccessTokenCookie(responseHeaders, newAccessToken);
        addRefreshTokenCookie(responseHeaders, newRefreshToken);

        whiteListRefreshToken(newRefreshToken.getTokenValue(), user);

        return ResponseEntity.ok().headers(responseHeaders).build();
    }

    public ResponseEntity<?> signOut(HttpServletRequest request, HttpServletResponse response) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        SecurityContextHolder.clearContext();
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                cookie.setMaxAge(0);
                cookie.setValue("");
                cookie.setHttpOnly(true);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        }
        User user = userRepository.findByUsername(username).orElseThrow(IllegalArgumentException::new);
        clearRefreshTokenWhiteList(user);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<?> refresh(String refreshToken, HttpServletRequest request) {
        boolean refreshTokenValid = jwtUtil.validateToken(refreshToken);
        if (!refreshTokenValid) {
            return ResponseEntity.badRequest().build();
        }

        String username = jwtUtil.getUsername(refreshToken);
        User user = userRepository.findByUsername(username).orElseThrow(IllegalArgumentException::new);

        if (!user.getRefreshTokenWhitelist().contains(refreshToken)){
            return ResponseEntity.badRequest().build();
        }

        TokenDto newAccessToken = jwtUtil.generateToken(user, request);
        TokenDto newRefreshToken = jwtUtil.generateRefreshToken(user);
        HttpHeaders responseHeaders = new HttpHeaders();
        addAccessTokenCookie(responseHeaders, newAccessToken);
        addRefreshTokenCookie(responseHeaders, newRefreshToken);

        return ResponseEntity.ok().headers(responseHeaders).build();
    }

    public ResponseEntity<?> verify(VerifyCodeRequest verifyCodeRequest, HttpServletRequest request) {
        if(!jwtUtil.validateToken(verifyCodeRequest.getToken())){
            throw new IllegalArgumentException("Token expired.");
        }

        User user = userRepository
                .findByUsername(jwtUtil.getUsername(verifyCodeRequest.getToken()))
                .orElseThrow(IllegalArgumentException::new);

        if(!tfaUtil.verifyCode(verifyCodeRequest.getCode(), user.getSecret())) {
            throw new IllegalArgumentException("Code is incorrect");
        }

        if (!user.isMfa()){
            user.setMfa(true);
            userRepository.save(user);
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        TokenDto newAccessToken;
        TokenDto newRefreshToken;

        newAccessToken = jwtUtil.generateToken(user, request);
        newRefreshToken = jwtUtil.generateRefreshToken(user);
        addAccessTokenCookie(responseHeaders, newAccessToken);
        addRefreshTokenCookie(responseHeaders, newRefreshToken);

        whiteListRefreshToken(newRefreshToken.getTokenValue(), user);

        return ResponseEntity.ok().headers(responseHeaders).build();
    }

    private void addAccessTokenCookie(HttpHeaders httpHeaders, TokenDto token) {
        httpHeaders.add(HttpHeaders.SET_COOKIE, cookieUtil.createAccessTokenCookie(token.getTokenValue(), token.getDuration()).toString());
    }

    private void addRefreshTokenCookie(HttpHeaders httpHeaders, TokenDto token) {
        httpHeaders.add(HttpHeaders.SET_COOKIE, cookieUtil.createRefreshTokenCookie(token.getTokenValue(), token.getDuration()).toString());
    }

    private void whiteListRefreshToken(String refreshToken, User user){
        if (user.getRefreshTokenWhitelist() == null){
            user.setRefreshTokenWhitelist(new ArrayList<>());
        }
        user.getRefreshTokenWhitelist().add(refreshToken);
        userRepository.save(user);
    }

    private void clearRefreshTokenWhiteList(User user){
        user.setRefreshTokenWhitelist(new ArrayList<>());
        userRepository.save(user);
    }
}
