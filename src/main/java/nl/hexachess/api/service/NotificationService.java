package nl.hexachess.api.service;

import nl.hexachess.api.model.dto.Dto;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

@Service
public class NotificationService {
    private final Sinks.Many<Dto> sink;

    public NotificationService() {
        this.sink = Sinks.many().multicast().onBackpressureBuffer();
    }

    public Sinks.EmitResult publish(Dto object){
        return sink.tryEmitNext(object);
    }

    public Flux<Dto> getSink(){
        return sink.asFlux();
    }
}
