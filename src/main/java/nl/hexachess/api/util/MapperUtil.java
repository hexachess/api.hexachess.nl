package nl.hexachess.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;

public abstract class MapperUtil {

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final ModelMapper modelMapper = new ModelMapper();

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static ModelMapper getModelMapper() {
        return modelMapper;
    }
}
