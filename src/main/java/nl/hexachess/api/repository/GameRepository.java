package nl.hexachess.api.repository;

import nl.hexachess.api.model.entity.Game;
import nl.hexachess.api.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
}
