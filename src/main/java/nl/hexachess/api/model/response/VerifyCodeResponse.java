package nl.hexachess.api.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.hexachess.api.model.dto.TokenDto;

@Data
@AllArgsConstructor
public class VerifyCodeResponse {
    private TokenDto token;
    private String image;
    private String secret;
}
