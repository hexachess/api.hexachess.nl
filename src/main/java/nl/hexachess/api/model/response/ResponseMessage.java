package nl.hexachess.api.model.response;

import lombok.Data;

@Data
public class ResponseMessage {
    private Boolean success;
    private String message;
}
