package nl.hexachess.api.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class VerifyCodeRequest {
    @NotBlank
    private String code;
    @NotBlank
    private String token;
}
