package nl.hexachess.api.model.dto;

import lombok.Data;

@Data
public class UserDto {
    private String username;
    private String email;
}
