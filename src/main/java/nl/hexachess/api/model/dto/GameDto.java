package nl.hexachess.api.model.dto;

import lombok.Data;

@Data
public class GameDto extends Dto {
    private Long id;
    private int mode;
}
