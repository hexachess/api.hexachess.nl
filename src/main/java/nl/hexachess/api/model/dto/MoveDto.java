package nl.hexachess.api.model.dto;

import lombok.Data;

@Data
public class MoveDto extends Dto {
    private GameDto game;
    private UserDto player;
    private int oldX;
    private int oldY;
    private int newX;
    private int newY;
}
