package nl.hexachess.api.controller;

import lombok.RequiredArgsConstructor;
import nl.hexachess.api.model.request.LoginRequest;
import nl.hexachess.api.model.request.SignUpRequest;
import nl.hexachess.api.model.request.VerifyCodeRequest;
import nl.hexachess.api.security.util.SecurityCipher;
import nl.hexachess.api.service.AuthService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.Session;
import org.springframework.session.security.SpringSessionBackedSessionRegistry;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;

@CrossOrigin
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final FindByIndexNameSessionRepository sessionRepository;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(
            @CookieValue(name = "accessToken", required = false) String accessToken,
            @CookieValue(name = "refreshToken", required = false) String refreshToken,
            @Valid @RequestBody LoginRequest loginRequest,
            HttpServletRequest request) {
        signOutAllSessions(loginRequest.getUsername());
        String decryptedAccessToken = SecurityCipher.decrypt(accessToken);
        String decryptedRefreshToken = SecurityCipher.decrypt(refreshToken);
        return authService.signIn(loginRequest, decryptedAccessToken, decryptedRefreshToken, request);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signupRequest, HttpServletRequest request) {
        return authService.registerUser(signupRequest, request);
    }

    @GetMapping("/signout")
    public ResponseEntity<?> signOut(HttpServletRequest request, HttpServletResponse response) {
        return authService.signOut(request, response);
    }

    @PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> refreshToken(@CookieValue(name = "refreshToken", required = false) String refreshToken, HttpServletRequest request) {
        String decryptedRefreshToken = SecurityCipher.decrypt(refreshToken);
        return authService.refresh(decryptedRefreshToken, request);
    }

    @PostMapping(value = "/verify", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> verfiyCode(@Valid @RequestBody VerifyCodeRequest verifyCodeRequest, HttpServletRequest request) {
        return authService.verify(verifyCodeRequest, request);
    }

    private void signOutAllSessions(String pricipalName) {
        SpringSessionBackedSessionRegistry sessionRegistry = new SpringSessionBackedSessionRegistry(sessionRepository);
        Collection<? extends Session> userSessions = sessionRepository.findByIndexNameAndIndexValue(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME, pricipalName).values();
        userSessions.forEach((session) -> sessionRegistry.getSessionInformation(session.getId()).expireNow());
    }
}
