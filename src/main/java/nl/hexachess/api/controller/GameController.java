package nl.hexachess.api.controller;

import lombok.RequiredArgsConstructor;
import nl.hexachess.api.model.dto.Dto;
import nl.hexachess.api.model.dto.GameDto;
import nl.hexachess.api.model.dto.MoveDto;
import nl.hexachess.api.model.entity.Game;
import nl.hexachess.api.service.GameService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/game")
@RequiredArgsConstructor
public class GameController {
    private final GameService gameService;

    @GetMapping("{id}")
    public ResponseEntity<Game> getGame(@PathVariable Long id){
        return gameService.getGame(id);
    }

    @GetMapping("join/{id}")
    public ResponseEntity<Game> joinGame(@PathVariable Long id){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return gameService.joinGame(id, username);
    }

    @PostMapping
    public ResponseEntity<Game> createGame(@RequestBody GameDto gameDto){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return gameService.createGame(gameDto, username);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteGame(@PathVariable Long id){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return gameService.removeGame(id, username);
    }

    @GetMapping("/overview/{username}")
    public ResponseEntity<List<Game>> getGames(@PathVariable String username){
        return gameService.getGames(username);
    }

    @PostMapping("{id}")
    public ResponseEntity<Game> placeMove(@RequestBody MoveDto moveDto, @PathVariable Long id){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return gameService.addMove(id, moveDto, username);
    }

    @GetMapping(path = "moveupdates/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Dto> getGameMoveUpdates(@PathVariable Long id){
        return gameService.getMoveUpdates(id);
    }

    @GetMapping(path = "joinupdates/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Dto> getGameJoinUpdates(@PathVariable Long id){
        return gameService.getJoinUpdates(id);
    }
}
