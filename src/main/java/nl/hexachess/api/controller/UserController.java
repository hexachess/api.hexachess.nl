package nl.hexachess.api.controller;

import lombok.RequiredArgsConstructor;
import nl.hexachess.api.model.dto.UserDto;
import nl.hexachess.api.model.entity.User;
import nl.hexachess.api.repository.UserRepository;
import nl.hexachess.api.util.MapperUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserRepository userRepository;

    @GetMapping
    public ResponseEntity<UserDto> getUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findByUsername(username).orElseThrow(IllegalArgumentException::new);
        UserDto userDto = MapperUtil.getModelMapper().map(user, UserDto.class);
        return ResponseEntity.ok(userDto);
    }
}
