package nl.hexachess.api.security;

import lombok.RequiredArgsConstructor;
import nl.hexachess.api.model.entity.User;
import nl.hexachess.api.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        return UserDetailsImpl.builder().id(user.getId()).username(user.getUsername()).email(user.getEmail()).password(user.getPassword()).build();
    }

    //TODO: remove??
    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).
                orElseThrow(() -> new UsernameNotFoundException("User not found")
                );
        return UserDetailsImpl.builder().id(user.getId()).username(user.getUsername()).email(user.getEmail()).password(user.getPassword()).build();
    }
}
